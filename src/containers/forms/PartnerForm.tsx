import * as React from "react";
import {Partnerships} from "../../components/forms/Partnerships";

export class PartnerForm extends React.Component<{}, {}> {
    constructor() {
        super ();
    }

    handleSubmitForm() {
        console.log("submit form");
    }

    render() {
        return (
            <Partnerships handleSubmit={() => { this.handleSubmitForm}} />
        );
    }
}