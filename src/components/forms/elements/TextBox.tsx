import * as React from "react";
const styles = require("./TextBox.css");
export interface TextBoxProps {
    altValue: string,
    name: string,
    onBlur?:(e: any) => void,
    passVal?: string,
    type?: string,
    validate?: boolean,
    value?: string,
}
export const TextBox = (props: TextBoxProps) => {
    let type = props.type ? props.type : "text";
        if (props.passVal === "") {
            return (
                <div>
                <i className="fa fa-check" aria-hidden="true" style={{ color: "green"}}></i>
                    <input alt={props.altValue}
                           className={styles.TextBox + " col-xs-12"}
                           onBlur={props.onBlur}
                           name={props.name}
                           placeholder={props.altValue}
                           style={{ marginLeft: "20px"}}
                           type={type}
                           value={props.value}
                    />
                </div>
            );
        } else if (props.passVal === "fail") {
            return (
                <div>
                <i className="fa fa-close" aria-hidden="true" style={{ color: "red", fontSize: "20px"}}></i>
                    <input alt={props.altValue}
                           className={styles.TextBox + " col-xs-12"}
                           onBlur={props.onBlur}
                           name={props.name}
                           placeholder={props.altValue}
                           style={{ marginLeft: "20px"}}
                           type={type}
                           value={props.value}
                    />
                </div>
            );
        } else {
            return (
                <div>
                    <input alt={props.altValue}
                           className={styles.TextBox + " col-xs-12"}
                           onBlur={props.onBlur}
                           name={props.name}
                           placeholder={props.altValue}
                           style={{ marginLeft: "20px"}}
                           type={type}
                           value={props.value}
                    />
                </div>
            );
        }

}