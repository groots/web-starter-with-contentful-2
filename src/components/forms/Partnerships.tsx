import * as React from "react";
import { MultiSelectBox} from "../../containers/forms/elements/MultiSelectBox";
import { InputText } from "../../containers/forms/elements/InputText";
import { ButtonManager } from "../../containers/forms/elements/ButtonManager";

export const Partnerships = (props: any) => {
    return (
        <form method="Post" className="container">
            <div className="row" style={{margin: "20px 30px"}}>
                <div className="pull-left col-md-6">
                <InputText name="FirstName" validate={true} altValue="First Name" />
                </div>
                <div className="pull-right col-md-6">
                <InputText name="LastName" validate={true} altValue="Last Name" />
                </div>
            </div>
            <div className="row" style={{margin: "20px 30px"}}>
                <div className="pull-left col-md-6">
                <InputText name="Company" validate={false} altValue="Company" />
                </div>
                <div className="pull-right col-md-6">
                <InputText name="WebAddress" validate={true} altValue="Web Address" />
                </div>
            </div>
            <div className="row" style={{margin: "20px 30px"}}>
                <div className="pull-left col-md-6">
                    <InputText name="Phone Number" validate={true} altValue="Phone Name" />
                </div>
                <div className="pull-right col-md-6">
                    <MultiSelectBox name="Candy Specialty" validate={true} altValue="First Name" />
                </div>
            </div>
            <div className="row" style={{margin: "20px 30px"}}>
                <ButtonManager name="PartnerSubmit" value="Become A Partner" buttonAction={props.handleSubmit} />
            </div>

        </form>
    );
}