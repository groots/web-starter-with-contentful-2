var webpack = require('webpack');
var WebpackDevServer = require('webpack-dev-server');

var env = process.env.ENV;
if (env === "prd") {
  var config = require('../config/webpack.config.prd');
} else if (env === "uat") {
  var config = require('../config/webpack.config.uat');
} else if (env === "int") {
  var config = require('../config/webpack.config.int');
} else {
  var config = require('../config/webpack.config.dev');
}

new WebpackDevServer(webpack(config), {
  publicPath: config.output.publicPath,
  hot: true,
  historyApiFallback: true
}).listen(3000, 'localhost', (err) => {
  if (err) {
    return console.log(err);
  }

  console.log('Listening at http://localhost:3000');
});
